$(window).on('load', function() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    };
    $('body').removeClass('loaded');

    if ($('.advantage-list').length) {
        setTimeout(function(){            
            $('.advantage-list').addClass('animation-item');
        }, 1500);   
    }
        /*map contact page begin*/


});

/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function() {
    if ($('.js-lazy').length) {
        $('.js-lazy').lazy();
    }
    if ($('.js-town').length) {
        var availableTags = [
            "Москва",
            "МО Москва",
            "Мурманск",
            "Магнитагорск"
        ];
        $('.js-town').autocomplete({
            source: availableTags
        });

    };
    /*button open main nav begin*/
    $('.js-button-nav').click(function() {
        $(this).toggleClass('active');
        $('.main-nav-mob').toggleClass('show-nav');
        $('.content').toggleClass('hover-nav-mob');
        return false;
    });
    $('.nav-overlay').click(function() {
        $('.js-button-nav').removeClass('active');
        $('.main-nav-mob').removeClass('show-nav');
        $('.content').removeClass('hover-nav');
    });
    $('.nav-overlay').click(function() {
        $('.js-button-nav').removeClass('active');
        $('.main-nav-mob').removeClass('show-nav');
        $('.content').removeClass('hover-nav-mob');
    });

    /*button open main nav end*/
    /*catalog main begin*/
    var timer;
    $('.js-drop-nav').hover(function() {
        var dropNav = $(this).find('a').attr('href');
        var activeNav = $(this);
        timer = setTimeout(function() {
            $(dropNav).addClass('active-nav');
            $(activeNav).siblings().removeClass('active');
            $(activeNav).addClass('active');
            $(dropNav).siblings().removeClass('active-nav');
        }, 400);
    }, function() {
        clearTimeout(timer);
    });
    $('.js-drop-nav a').click(function() {
        var $el = $(this),
            dropNav = $el.attr('href');
        $el.parent().siblings().removeClass('active');
        $el.parent().addClass('active');
        $(dropNav).addClass('active-nav').siblings().removeClass('active-nav');

        if (window.innerWidth < 768) {
            $("html, body").animate({ scrollTop: $el.closest('.js-with-drop').offset().top });
        }
        return false;
    });
    var timer2;
    $('.drop-nav__col_1 .js-drop-nav').hover(function() {
        timer2 = setTimeout(function() {
            $('.drop-nav__col_3 .js-drop-nav').removeClass('active');
            $('.drop-nav__col_3 .drop-sub-nav').removeClass('active-nav');
        }, 400);
    }, function() {
        clearTimeout(timer2);
    });
    $('.js-with-drop').hover(function() {
        var itemNav = $(this);
        timer = setTimeout(function() {
            $(itemNav).addClass('hover');
            $('.content').addClass('hover-nav');
            if (window.innerWidth < 768) {
                 $(this).closest('.js-with-drop').siblings().addClass('hide');
            }
        }, 400);
    }, function() {
        clearTimeout(timer);
        $('.js-with-drop').removeClass('hover');
        $('.content').removeClass('hover-nav');
    });
    $('.js-with-drop .main-nav-list__link').click(function() {
        $(this).parent().toggleClass('hover');
        $('.content').toggleClass('hover-nav');
        return false;
    });

    $('.drop-nav__col_1 .js-drop-nav a').click(function() {
        $(this).parents('.drop-nav__col_1').next().addClass('active-col');
        $(this).parents('.drop-nav__col_1').addClass('this-active-col');
        return false;
    });
    $('.drop-nav__col_2 .js-drop-nav a').click(function() {
        $(this).parents('.drop-nav__col_2').next().addClass('active-col');
        $(this).parents('.drop-nav__col_2').addClass('this-active-col');
        return false;
    });
    if (window.innerWidth < 768) {
        $('.js-back-nav').click(function() {
            $(this).parents('.drop-nav__col').removeClass('active-col');
            $(this).parents('.drop-nav__col').prev().removeClass('this-active-col');
            $(this).parents('.drop-nav__col').find('.drop-sub-nav').removeClass('active-nav');
            $(this).parents('.drop-nav__col').find('.js-drop-nav').removeClass('active');
            return false;
        });
    }
    /*catalog main end*/
    $('.js-back-nav-menu').on('click',function() {
        $(this).closest('.main-nav-list__item').siblings().removeClass('hide');
        $(this).closest('.main-nav-list__item').removeClass('hover');
        $('.content').toggleClass('hover-nav');
        return false;
    });

    /* anchor link begin*/
    $('.js-anchor').click(function() {
        var $el = $(this),
            $anchor = $el.attr('href');

        $("html, body").animate({ scrollTop: $($anchor).offset().top });
        return false;
    });
    
    /* anchor link end*/

    /*tabs begin*/
    $('.js-tabs li a').click(function() {
        $(this).parents('.js-tab-wrap').find('.tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
        if ($(id).find('.js-styled').length > 0) {
           $(id).find('.js-styled').trigger('refresh') 
        }
        return false;
    });
    /* tabs end*/
    /*accordion begin*/
    $('.js-accordion-title').click(function() {
        var $el = $(this);
        $el.parent().siblings().find('.js-accordion-title').removeClass('active');
        $el.parent().siblings().find('.js-accordion-cont').slideUp();
        $el.toggleClass('active');
        $el.parent().find('.js-accordion-cont').slideToggle(300, function() {
            if ($el.closest('.fancybox-wrap').length) {
                $.fancybox.update();
            } 
        });
        return false;
    });
    /*accordion end*/
    /*lk mob accordion begin*/
    $('.js-lk-mob-title').click(function() {
        $(this).toggleClass('active');
        $(this).parent().toggleClass('opened');
        $('.nav-overlay').toggleClass('lk-opened');
        return false;
    });
    $('.nav-overlay').click(function() {
        $('.js-lk-mob-title').removeClass('active');
        $('.lk-nav').removeClass('opened');
        $('.nav-overlay').removeClass('lk-opened');
        return false;
    });
    /*lk mob accordion end*/
    /*mob table on news page begin*/
    $('.js-table-text-title').click(function() {
        $(this).toggleClass('active');
        $(this).parent().find('table').slideToggle();
        return false;
    });
    $(window).resize(function() {
        if (window.innerWidth > 576 && $('.table-text-list-wrap').find('table').length > 0 && $('.table-text-list-wrap').find('table').is(':hidden')) {
            $('.table-text-list-wrap').find('table').css('display','');
        }
    });
    /*mob table on news page end*/
    /*delivery choise begin*/
    $('.list-radio input').live('change', function() {
        if ($(this).is(":checked")) {
            var itemChecked = $(this).attr('data-delivery');
            $('.delivery-logo').addClass('hide-tab');
            $(itemChecked).removeClass('hide-tab');
        };
    });    
    /*delivery choise begin*/    
    /*checkout select begin*/
    $('.js-checkout-title').click(function() {
        $(this).parents('.checkout-step__item').siblings().find('.js-checkout-title').removeClass('active'); 
        $(this).parents('.checkout-step__item').siblings().removeClass('open-drop');
        $(this).toggleClass('active');
        $(this).parents('.checkout-step-select').toggleClass('open-drop');

        return false;
    });
    $(document).on('touchstart click', function(e) {
       if ($(e.target).parents().filter('.checkout-step-select:visible').length != 1) {
           $('.checkout-step-select').removeClass('open-drop');
            $('.js-checkout-title').removeClass('active');
       }
       if ($('.show-nav').length) {
        if ($(e.target).closest('.main-nav-mob').length || $(e.target).closest('.js-button-nav').length) {
            return;
        } else {
            $('.js-button-nav').trigger('click');
            $('.main-nav-list__item').removeClass('hide')
        }
       }

       if ($('.ui-datepicker').is(':visible')) {
            if ($(e.target).closest('.ui-datepicker').length || $(e.target).is('#date-range')) {
                return;
            } else {
                $('#date-range').trigger('blur')
                $('#date-range').datepicker( "hide" )
            }
       }
       if ($('#searchMob').is(':visible') && window.innerWidth < 768) {
            if ($(e.target).closest('#searchMob').length || $(e.target).closest('.js-search').length || $(e.target).closest('#searchResults').length) {
                return;
            } else {
                $('#searchMob').hide();
                if ($('body').is('.open-search')) {
                    $('#searchResults').hide();
                    $('body').removeClass('open-search scrollDisabled')
                }
            }
       }
       if ($('body').is('.open-search')) {
            if ($(e.target).closest('#search').length || $(e.target).closest('.js-search').length || $(e.target).closest('#searchResults').length) {
                return;
            } else {
                    $('#searchResults').hide();
                    $('body').removeClass('open-search scrollDisabled')
            }
       }
        
   });
    /*checkout select end*/
    /*custom select begin*/
    $('.js-custom-select').click(function() {
        $('.js-custom-select').not(this).removeClass('active'); 
        $('.custom-select:not(:has(.opened))').removeClass('opened');       
        $(this).toggleClass('active');
        $(this).parents('.custom-select').toggleClass('opened');
        return false;
    });
    $('.custom-select-list__title').click(function() {
        var textLink = $(this).text();
        $(this).parents('.custom-select').find('.custom-select__title span').text(textLink);
        $(this).parents('li').siblings().removeClass('selected');
        $(this).parents('li').addClass('selected');
        $('.custom-select').removeClass('opened');
        return false;
    });
    $('.custom-select-list__remove').click(function() {
        if ($(this).parents('li').hasClass("selected")) {
            $(this).parents('li').prev().addClass('selected');
            var textLink = $(this).parents('li').prev().find('.custom-select-list__title').text();
            $(this).parents('.custom-select').find('.custom-select__title span').text(textLink);
        }
        $(this).parents('li').remove();
        $('.custom-select').removeClass('opened');
        return false;
    });    
    $(document).on('touchstart click', function(e) {
        if ($(e.target).parents().filter('.custom-select:visible').length != 1) {
            $('.custom-select').removeClass('opened');
        }
    });
    /*custom select end*/ 
    /*promo mob title begin*/
    $('.js-promo-mob-title').click(function() {
        $(this).toggleClass('active');
        $(this).parents('.promo-sort').find('.brands-tabs').slideToggle();
        return false;
    });
    /*promo mob title end*/  
    /*filter aside accordion begin*/    
    $('.js-aside-title').click(function() {
        $(this).toggleClass('active');
        $(this).parents('.aside-filter__item').find('.aside-filter__cont').toggleClass('hide-tab');
        return false;
    });
    /*filter aside accordion end*/
    /*tooltipe filter begin*/    
    $('.js-tooltipe-filter').click(function() {
        $(this).parent().toggleClass('opened');
        return false;
    });
    $(document).on('touchstart click', function(e) {
        if ($(e.target).parents().filter('.tooltipe-filter:visible').length != 1) {
            $('.tooltipe-filter').removeClass('opened');
        }
    });
    /*tooltipe filter end*/   
    /*show mob filter begin*/    
    $('.js-show-mob-filter').click(function() {
        $('.aside-filter').toggleClass('opened');
        return false;
    });
    $(document).on('click', function(e) {
        if ($(e.target).parents().filter('.aside-filter-col:visible').length != 1) {
            //$('.aside-filter').removeClass('opened');
        }
    });
    var ts;
    $(document).on('touchstart', function (e){
        if ($(e.target).parents().filter('.aside-filter-col:visible').length != 1) {
            ts = e.originalEvent.touches[0].clientY;
        }
    });

    $(document).on('touchend', function (e){
        if ($(e.target).parents().filter('.aside-filter-col:visible').length != 1) {
           var te = e.originalEvent.changedTouches[0].clientY;
           if(      ts <= te + 10 && (ts + 10) >= te ){
              $('.aside-filter').removeClass('opened');
           }
       }
    });
     /*show mob filter end*/




    /* components begin*/


    if ($('.js-slider').length) {
        $('.js-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right-3"></i></button>',
        });
    };
    if ($('.js-promo-slider').length) {
        $('.js-promo-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            fade: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right-3"></i></button>',
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        variableWidth: true,
                        arrows: false
                    }
                }
            ]
        });
    };
    if ($('.js-news-slider').length) {
        $('.js-news-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right-3"></i></button>',
            responsive: [{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: "unslick"
                }
            ]
        });
    };

    if ($('.js-popup-slider').length) {
        $('.js-popup-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            fade: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right-3"></i></button>'
        });
    };

    var y_offsetWhenScrollDisabled;
    $(window).scroll(function () {
        y_offsetWhenScrollDisabled = $(window).scrollTop();
    })

    function lockScroll() {
        offset = y_offsetWhenScrollDisabled;
        $('body').addClass('scrollDisabled');
        $('html').css('margin-top', -y_offsetWhenScrollDisabled);

    }

    function unlockScroll() {
        $('body').removeClass('scrollDisabled');
        $('html').css('margin-top', 0);
        $('html, body').animate({
            scrollTop: offset
        }, 0);
    }


    if ($('.js-fancybox').length) {
        var $fancyoffset = 35;
        if (window.innerWidth < 340) {
            $fancyoffset= 15
        }
        $('.js-fancybox').fancybox({
            margin: $fancyoffset,
            padding: 0,
            autoCenter : true,
            beforeShow: function() {
                lockScroll();

            },
            afterShow: function() {
                if ($('.js-popup-slider').length) {
                    $('.js-popup-slider').slick('setPosition');
                };
            },
            afterClose: function() {
                unlockScroll();
            }
        });
    };



    if($('.js-styled').length) {
        $('.js-styled').styler();
    };
    if($('.js-styled-scroll').length) {
        $(".js-styled-scroll").mCustomScrollbar({
            axis:"y",
            theme:"rounded-dots",
            autoExpandScrollbar:true,
            advanced:{autoExpandHorizontalScroll:true}
        });
    }; 
    if($('.js-scroll-table').length) {
        $(".js-scroll-table").mCustomScrollbar({
            axis:"x",
            theme:"rounded-dots",
            autoExpandScrollbar:true,
            advanced:{autoExpandHorizontalScroll:true}
        });
    }; 
    
    if($('.js-upload').length) {
        $(".js-upload").MultiFile({
            list: '#upload-list'
        });
    };  

    var $resultsFilter = $('#resultsFilter');
    if($('#slider-1').length) {
        $("#slider-1").slider({
            value:5,
            min: 0,
            max: 5000,
            step: 50,
            slide: function( event, ui ) {
                $("#slider-1-value" ).val( ui.value );
                if ($resultsFilter.length) {
                    posResultsRange($(this))
                }
            }
        })
    };  
    if($('#slider-2').length) {
        $("#slider-2").slider({
            value:9999,
            min: 5000,
            max: 10000,
            step: 50,
            slide: function( event, ui ) {
                $("#slider-2-value" ).val( ui.value );
                if ($resultsFilter.length) {
                    posResultsRange($(this))
                }
            }
        })
    };
    function posResultsRange(el) {
        var $el = el, 
            $parent = $el.closest('.js-filter-wrap'),
            $offset = $parent.position().top + $parent.outerHeight()/2 - $resultsFilter.outerHeight()/2;

        $resultsFilter.css({
            top: $offset
        })
        if ($resultsFilter.is(':hidden')) {
            $resultsFilter.fadeIn(300)
        }
    }

    if ($('#date-range').length) {
        $('#date-range').datepicker({
            range: 'period',
            numberOfMonths: 2,
                closeText: 'Закрыть',
                prevText: '',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                    'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                    'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',  
                autoClose: true,       
            dateFormat: 'dd.mm.y',
            onSelect: function(dateText, inst, extensionRange) {
                $('#date-range').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
                if (extensionRange.endDateText != extensionRange.startDateText) {
                    $('#date-range').datepicker( "hide" )
                }
            }
        }); 
    };


    /* components end*/
   if ($('.js-mask-tel').length) { 
        $('.js-mask-tel').inputmask("+7 999 999-99-99",{ "clearIncomplete": true } );
    }

    $('#search, .search-box').on('keydown', function(){
        $('#searchResults').show();
        $('body').addClass('open-search scrollDisabled');
        if (!$('#searchResults').is('.mCustomScrollbar')) {
            $("#searchResults").mCustomScrollbar({
                axis:"y",
                //theme:"rounded-dots",
                autoExpandScrollbar:true,
                advanced:{autoExpandHorizontalScroll:true}
            });
        }
    });
    $('#resetForm').on('click', function(){
        $('#searchResults').hide();
        $('body').removeClass('open-search scrollDisabled')

    });


    function maxHeightSearch() {

        if ($('#searchResults').length ) {
            $('#searchResults').css({
                'max-height': window.innerHeight - $('header').outerHeight() + parseFloat($('#searchResults').css('padding-top')) 
            });
        }
    }
    maxHeightSearch()
    $(window).resize(function() {
        maxHeightSearch();
        if ($('#searchResults').length && $('#searchResults').is('.mCustomScrollbar')) {
            $('#searchResults').find('.mCustomScrollBox').css('max-height', window.innerHeight - $('header').outerHeight() + parseFloat($('#searchResults').css('padding-top')));
        }

        if (window.innerWidth <= 480 &&$('#searchResults').length && $('body').is('.open-search')) {
            $('#searchMob').show();
        }
    })
    if (window.innerWidth < 481) {
        $('.js-search').on('click', function() {
            $('#searchMob').show();
            $(this).find('.search__input').trigger('blur');
            $('.search-box').trigger('focus')
        });
    }
    $('.js-close-search').on('click', function() {
        $('#searchMob').hide();
        $('#searchResults').hide();
        $('body').removeClass('open-search scrollDisabled')
    });

        // filters results position
        $('.js-filter-item').on('click', function() {
            var $el = $(this),
                
                $parent = $el.closest('.js-filter-wrap'),
                $offset = $parent.position().top + $parent.outerHeight()/2 - $resultsFilter.outerHeight()/2;
            if ($parent.closest('.js-styled-scroll').length) {
                $offset = $parent.closest('.js-styled-scroll').position().top  + $parent.position().top + parseFloat($parent.closest('.js-styled-scroll').find('.mCSB_container').css('top')) + $parent.outerHeight()/2 - $resultsFilter.outerHeight()/2
             
            }
            if ($parent.is('.filter-radios')) {
                $offset = $offset + parseFloat($parent.css('margin-top'))
            }
            $('.js-filter-wrap').removeClass('active-filter')
            $parent.addClass('active-filter');
            $resultsFilter.css({
                top: $offset
            })
            if ($resultsFilter.is(':hidden')) {
                $resultsFilter.fadeIn(300)
            }
        });

        $(window).resize(function() {
            if (window.innerWidth < 1200 && $resultsFilter.length && !$('#opened').is('.opened')) {
                $resultsFilter.hide()
            }
            if ($resultsFilter.length) {
                filtersInside()
            }
        });
        function filtersInside() {

            if (window.innerWidth < 481) {
                if ($resultsFilter.parent().is('#filter')) {
                   var $content = $resultsFilter.detach();
                   $content.appendTo('#filtersList');
                }

            } else {
                if ($resultsFilter.parent().is('#filtersList')) {
                   var $content = $resultsFilter.detach();
                   $content.appendTo('#filter');
                }
            }
        }
            if ($resultsFilter.length) {
                filtersInside()
            }
        $('#filtersList').scroll(function() {
            if ($resultsFilter.is(':visible') && window.innerWidth < 1200 && window.innerWidth > 480) {
                var $parent = $('.js-filter-wrap').filter('.active-filter')
                var $offset = $parent.position().top + $parent.outerHeight()/2 - $resultsFilter.outerHeight()/2 - $('#filtersList').scrollTop();
                if ($parent.closest('.js-styled-scroll').length) {
                    $offset = $parent.closest('.js-styled-scroll').position().top  + $parent.position().top + parseFloat($parent.closest('.js-styled-scroll').find('.mCSB_container').css('top')) + $parent.outerHeight()/2 - $resultsFilter.outerHeight()/2
                }
                if ($parent.is('.filter-radios')) {
                    $offset = $offset + parseFloat($parent.css('margin-top'))
                }
                if ($offset < 0 ) {
                    $offset = 0; 
                } 
                if ($offset >  $('#filtersList').outerHeight()) {
                    $offset = $('#filtersList').outerHeight() - $resultsFilter.outerHeight()/2
                }
                $resultsFilter.css({
                    top: $offset
                })
                $resultsFilter.css({
                    'top': $offset
                })
            }

        })
        // filters results position
});
$(window).resize(function() {
    if (window.innerWidth >= 768) {
        if ($('.js-news-slider').length) {
            $('.js-news-slider').slick("getSlick").refresh();
        };
    }    
})
var handler = function() {
    var height_footer = $('footer').height();
    var height_header = $('header').height();
    //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});	
    var viewport_wid = viewport().width;
    var viewport_height = viewport().height;

    if (viewport_wid <= 1100) {

        if ($('.js-placeholder').length) {
            $('.js-placeholder').attr('placeholder', 'Поиск');
        };
    } else {
        if ($('.js-placeholder').length) {
            $('.js-placeholder').attr('placeholder', 'Поиск среди 250 000 товаров');
        };
    }
        /* placeholder*/
    $('input, textarea').each(function() {
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function() {
            $(this).attr('placeholder', '');
        });
        $(this).focusout(function() {
            $(this).attr('placeholder', placeholder);
        });
    });
    /* placeholder*/
    var compLen = (".js-faset-table").length;
    if(compLen>0){  
        $(".js-faset-table").find(".table-list__item").css("height","auto");

        for  (var j=1; j<=$('.box-faset-table__left').find('.js-block').length; j++){
            var $el = $(".js-faset-table").find('.js-block:nth-child('+j+')').find('.js-items');
            for (var i=1; i<99; i++){
                var height2 = 0;    
                $el.find('.table-list__item:nth-child('+i+')').each(function() {height2 = height2 > $(this).height() ? height2 : $(this).height();});   
                $el.find('.table-list__item:nth-child('+i+')').each(function() {$(this).css("height",height2+"px")});   
            }   
        }           
            setTimeout(function(){
                $(".js-faset-table").find(".table-list__item").css("height","auto");
                for  (var j=1; j<=$('.box-faset-table__left').find('.js-block').length; j++){
                    var $el = $(".js-faset-table").find('.js-block:nth-child('+j+')').find('.js-items');
                    for (var i=1; i<99; i++){
                        var height2 = 0;    
                        $el.find('.table-list__item:nth-child('+i+')').each(function() {height2 = height2 > $(this).height() ? height2 : $(this).height();});   
                        $el.find('.table-list__item:nth-child('+i+')').each(function() {$(this).css("height",height2+"px")});           
                    }   
                }    
            }, 500); 

    }
    if ($('.js-accordion-cont:hidden').length && window.innerWidth > 767) {
        $('.js-accordion-cont:hidden').css({'display': ''})
    }

}
$(window).bind('load', handler);
$(window).bind('resize', handler);