$(window).bind("load", function() {


var tag = document.createElement("script");
tag.src = "http://api-maps.yandex.ru/2.1/?lang=ru_RU";
document.getElementsByTagName("footer")[0].appendChild(tag);
$.getScript("http://api-maps.yandex.ru/2.1/?lang=ru_RU").done(function (script, textStatus) {
    if ($('.map').length) {
        function init() {
            var myMap1 = new ymaps.Map('map1', {
                center: [56.851574, 60.572115],
                zoom: 16.5,
                controls: []
            },{suppressMapOpenBlock: true});
            var myMap2 = new ymaps.Map('map2', {
                center: [56.851574, 60.572115],
                zoom: 16.5,
                controls: []
            },{suppressMapOpenBlock: true});
            var myMap3 = new ymaps.Map('map3', {
                center: [55.648047, 37.734479],
                zoom: 16.5,
                controls: []
            },{suppressMapOpenBlock: true});
            var myMap4 = new ymaps.Map('map4', {
                center: [56.854374, 60.570435],
                zoom: 16.5,
                controls: []
            },{suppressMapOpenBlock: true});
            var myMap5 = new ymaps.Map('map5', {
                center: [56.851225, 60.577083],
                zoom: 16.5,
                controls: []
            },{suppressMapOpenBlock: true});
            var myMap6 = new ymaps.Map('map6', {
                center: [51.674688, 39.211000],
                zoom: 11.5,
                controls: []
            },{suppressMapOpenBlock: true});
            myPlacemark = new ymaps.Placemark(myMap1.getCenter(), {
                hintContent: ''
            });
            myPlacemark2 = new ymaps.Placemark(myMap2.getCenter(), {
                hintContent: ''
            });
            myPlacemark3 = new ymaps.Placemark(myMap3.getCenter(), {
                hintContent: ''
            });
            myPlacemark4 = new ymaps.Placemark(myMap4.getCenter(), {
                hintContent: ''
            });
            myPlacemark5 = new ymaps.Placemark(myMap5.getCenter(), {
                hintContent: ''
            });
            myPlacemark6 = new ymaps.Placemark([51.666625, 39.269981], {
                hintContent: ''
            }, {
                iconLayout: 'default#image',
                iconImageHref: 'img/map-marker.png',
                iconImageSize: [66, 72],
                iconImageOffset: [-33, -72]
            });
            myPlacemark7 = new ymaps.Placemark([51.690111, 39.146492], {
                hintContent: ''
            }, {
                iconLayout: 'default#image',
                iconImageHref: 'img/map-marker.png',
                iconImageSize: [66, 72],
                iconImageOffset: [-33, -72]
            });
            myMap1.geoObjects.add(myPlacemark);;
            myMap2.geoObjects.add(myPlacemark2);
            myMap3.geoObjects.add(myPlacemark3);
            myMap4.geoObjects.add(myPlacemark4);
            myMap5.geoObjects.add(myPlacemark5);
            myMap6.geoObjects.add(myPlacemark6);
            myMap6.geoObjects.add(myPlacemark7);
            if ($("body").hasClass("ios")) {
                myMap1.behaviors.disable('multiTouch');
                myMap1.behaviors.disable('scrollZoom');
                myMap1.behaviors.disable('drag');
            }
        }
        ymaps.ready(init);
    }
    if ($('#map-shop').length) {
        ymaps.ready(init);
        function init() {
            var myMap = new ymaps.Map('map-shop', {
                center: [56.8539596, 60.572115],
                zoom: 12.5,
                controls: []
            },{suppressMapOpenBlock: true});
            myPlacemark = new ymaps.Placemark([56.851574, 60.572115], {
                hintContent: ''
            }, {
                iconLayout: 'default#image',
                iconImageHref: 'img/map-marker.png',
                iconImageSize: [66, 72],
                iconImageOffset: [-33, -72]
            });
            myMap.geoObjects.add(myPlacemark);
            if ($("body").hasClass("ios")) {
                myMap.behaviors.disable('multiTouch');
                myMap.behaviors.disable('scrollZoom');
                myMap.behaviors.disable('drag');
            }
        }
    }

    if ($('#delivery-map').length) {
        ymaps.ready(init);
        function init() {
            var myMap1 = new ymaps.Map('delivery-map', {
                center: [56.838011, 60.597465],
                zoom: 12.5,
                controls: []
            },{suppressMapOpenBlock: true});
            if ($("body").hasClass("ios")) {
                myMap1.behaviors.disable('multiTouch');
                myMap1.behaviors.disable('scrollZoom');
                myMap1.behaviors.disable('drag');
            }
        }
    }
});
});